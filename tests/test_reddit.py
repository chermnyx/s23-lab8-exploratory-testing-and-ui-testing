# test_my_application.py
import pytest

from playwright.sync_api import Page

TIMEOUT = 10_000

def test_find_cats(page: Page) -> None:
    page.context.set_default_timeout(TIMEOUT)

    page.goto("https://reddit.com/")
    input = page.query_selector("#header-search-bar")
    assert input is not None

    input.fill("cat")
    assert input.input_value() == "cat"
    page.locator(
        ':text("Communities"):visible:below(#SearchDropdownContent)'
    ).first.wait_for()
    input.press("Enter")

    cats_community = page.get_by_test_id("communities-list").get_by_text(
        "r/cat", exact=True
    )
    cats_community.wait_for()
    cats_community.click()

    # check that the page of r/cat is opened
    title = page.locator('h1:text("Cat"):near(button:text("Join"))')
    title.wait_for()
    r = page.locator(':text("r/cat"):near(h1:text("Cat"):near(button:text("Join")))')
    r.wait_for()


def test_top_post(page: Page):
    page.context.set_default_timeout(TIMEOUT)

    page.goto("https://www.reddit.com/r/cat")

    TOP_LOCATOR = ":text-is('Top'):visible"
    page.locator(TOP_LOCATOR).click()
    page.locator(f":text-is('Today'):near({TOP_LOCATOR}):visible").click()
    page.locator(':text-is("All Time")').click()

    TITLE_SELECTOR = '[data-adclicklocation="title"]'
    page.wait_for_selector(TITLE_SELECTOR)
    post_title = page.query_selector(TITLE_SELECTOR)

    assert post_title is not None
    text = post_title.text_content()
    assert text is not None
    assert text.strip().startswith("Attempted to take a cute video of my cats licking each other")

def test_user_join_date(page: Page):
    page.context.set_default_timeout(TIMEOUT)

    page.goto("https://www.reddit.com/user/cwenham/")
    el = page.query_selector('#profile--id-card--highlight-tooltip--cakeday')
    assert el is not None
    text = el.text_content()
    assert text is not None

    assert text.strip() == 'February 28, 2006'
